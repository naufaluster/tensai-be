package com.ischyros.wallet.tensai.model;

import javax.persistence.*;

@Entity
@Table(name = "picture", schema = "tensai")
public class Picture {

    @Id
    @Column(name = "id_picture")
    private String idPicture;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "picture")
    private Byte[] picture;

    @ManyToOne
    @JoinColumn(name = "cif")
    private Customer cif;

    public String getIdPicture() {
        return idPicture;
    }

    public void setIdPicture(String idPicture) {
        this.idPicture = idPicture;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Byte[] getPicture() {
        return picture;
    }

    public void setPicture(Byte[] picture) {
        this.picture = picture;
    }

    public Customer getCif() {
        return cif;
    }

    public void setCif(Customer cif) {
        this.cif = cif;
    }

}
