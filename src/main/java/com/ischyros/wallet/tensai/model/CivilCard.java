package com.ischyros.wallet.tensai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "civil_card", schema = "tensai")
public class CivilCard {

    @Id
    @Column(name = "id_card")
    private String idCard;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "utilization")
    private Integer utilization;

    public String getidCard() {
        return idCard;
    }

    public void setidCard(String id_card) {
        this.idCard = id_card;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String first_name) {
        this.firstName = first_name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String last_name) {
        this.lastName = last_name;
    }

    public Integer getUtilization() {
        return utilization;
    }

    public void setUtilization(Integer utilization) {
        this.utilization = utilization;
    }

}
