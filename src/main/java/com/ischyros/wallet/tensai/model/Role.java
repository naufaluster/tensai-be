package com.ischyros.wallet.tensai.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "role", schema = "tensai")
public class Role {

    @Id
    @Column(name = "id_role")
    private Integer idRole;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "role")
    private Set<Customer> customer;

    public Integer getIdRole() {
        return idRole;
    }

    public void setIdRole(Integer idRole) {
        this.idRole = idRole;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Customer> getCustomer() {
        return customer;
    }

    public void setCustomer(Set<Customer> customer) {
        this.customer = customer;
    }
}
