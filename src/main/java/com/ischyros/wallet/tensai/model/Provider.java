package com.ischyros.wallet.tensai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "provider", schema = "tensai")
public class Provider {

    @Id
    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "first_name")
    private String firstName ;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "utilization")
    private Integer utilization;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getUtilization() {
        return utilization;
    }

    public void setUtilization(Integer utilization) {
        this.utilization = utilization;
    }
}
