package com.ischyros.wallet.tensai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TensaiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TensaiApplication.class, args);
	}

}
