package com.ischyros.wallet.tensai;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Main {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    public static void main(String[] args) {
        Random random = new Random();
        //method 1
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(timestamp);

        //method 2 - via Date
        Date date = new Date();
        System.out.println(new Timestamp(date.getTime()).toString());

        Long a = timestamp.getTime();
        //return number of milliseconds since January 1, 1970, 00:00:00 GMT
        System.err.println(timestamp.getTime());
        String id = String.format("%04d", a);
        //format timestamp
        System.out.println(id);

    }
}
