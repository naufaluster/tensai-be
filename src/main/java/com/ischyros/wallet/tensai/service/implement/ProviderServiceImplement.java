package com.ischyros.wallet.tensai.service.implement;

import com.ischyros.wallet.tensai.config.exception.NotFoundException;
import com.ischyros.wallet.tensai.model.Provider;
import com.ischyros.wallet.tensai.repository.ProviderRepository;
import com.ischyros.wallet.tensai.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProviderServiceImplement implements ProviderService {

    @Autowired
    private ProviderRepository providerRepo;

    @Override
    public Provider getByPhoneNumber(String phoneNumber) throws NotFoundException {
        Provider provider = providerRepo.findByPhoneNumber(phoneNumber);
        if (provider == null){
            throw new NotFoundException(44, "Phone Number does not exist in this world");
        }
        return provider;
    }
}
