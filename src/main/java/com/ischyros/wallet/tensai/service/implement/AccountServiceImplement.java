package com.ischyros.wallet.tensai.service.implement;

import com.ischyros.wallet.tensai.config.exception.UserException;
import com.ischyros.wallet.tensai.dto.AccountDTO;
import com.ischyros.wallet.tensai.model.Account;
import com.ischyros.wallet.tensai.service.AccountService;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImplement implements AccountService {

    private Account setAccount(AccountDTO dto){
        Account account = new Account();
        account.setAccountNumber(getAcnNumber());
        account.setAccountType(dto.getAccountType());
        account.setAccountName(dto.getAccountName());
        account.setBalance(dto.getBalance());
        account.setPin(dto.getPin());
        account.setCif(dto.getCif());
        return account;
    }

    private String getAcnNumber(){
        return null;
    }

    @Override
    public Account register(AccountDTO accountDTO) throws UserException {
        return null;
    }
}
