package com.ischyros.wallet.tensai.service.implement;

import com.ischyros.wallet.tensai.config.exception.NotFoundException;
import com.ischyros.wallet.tensai.config.exception.UserException;
import com.ischyros.wallet.tensai.dto.CustomerDTO;
import com.ischyros.wallet.tensai.dto.UpdateCustomerDTO;
import com.ischyros.wallet.tensai.model.*;
import com.ischyros.wallet.tensai.repository.CustomerRepository;
import com.ischyros.wallet.tensai.repository.PictureRepository;
import com.ischyros.wallet.tensai.service.CivilCardService;
import com.ischyros.wallet.tensai.service.CustomerService;
import com.ischyros.wallet.tensai.service.NpwpService;
import com.ischyros.wallet.tensai.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

@Service
public class CustomerServiceImplement implements CustomerService {

    @Autowired
    private CivilCardService civilCardService;

    @Autowired
    private NpwpService npwpService;

    @Autowired
    private ProviderService providerService;

    @Autowired
    private CustomerRepository customerRepo;

    @Autowired
    private PictureRepository pictureRepo;

    private Customer setCustomer(CustomerDTO dto){
        Customer customer = new Customer();
        customer.setCif(getCif(dto.getFirstName()));
        customer.setEmail(dto.getEmail());
        customer.setUsername(dto.getUsername());
        customer.setPassword(dto.getPassword());
        customer.setIdCard(dto.getIdCard());
        customer.setCashTag("$" + dto.getCashTag());
        customer.setFirstName(dto.getFirstName());
        customer.setLastName(dto.getLastName());
        customer.setNickName(dto.getFirstName());
        customer.setCountry(dto.getCountry());
        customer.setProvince(dto.getProvince());
        customer.setCity(dto.getCity());
        customer.setAddress(dto.getAddress());
        customer.setPostalCode(dto.getPostalCode());
        customer.setPhoneNumber(dto.getPhoneNumber());
        customer.setPlaceOb(dto.getPlaceOb());
        customer.setBirthDate(dto.getBirthDate());
        customer.setGender(dto.getGender());
        customer.setMother(dto.getMother());
        customer.setNpwp(dto.getNpwp());
        customer.setJob(dto.getJob());
        customer.setSalary(dto.getSalary());
        customer.setRole(1);
        customer.setCreatedAt(dto.getCreatedAt());
        customer.setUpdatedAt(dto.getUpdatedAt());
        return customer;
    }

    // Generate cif
    private String getCif(String fname){
        Random random = new Random();
        Calendar calendar = Calendar.getInstance();
        long timeMilli2 = calendar.getTimeInMillis();
        String ac = String.valueOf(timeMilli2);
        String df = ac.substring(9,13);
        String fs = fname.substring(0,1);
        BigInteger cs = customerRepo.countCustomer();
        int rand1 = random.nextInt((9999 - 1000) + 1) + 9999;
        String result = fs + cs.toString() + "90"+  "-" + df + "-" + rand1;
        return result;
    }

    // id card validation
    private boolean idCardValidation(String id_card, CustomerDTO dto) throws UserException, NotFoundException {
        Customer cs = customerRepo.findByIdCard(id_card);
        CivilCard cc = civilCardService.getById(id_card);
        if (dto.getIdCard() == null || dto.getIdCard().equalsIgnoreCase(" ")){
            throw new UserException(24, "Id Card column must be filled");
        }
        if(cc.getUtilization().equals(0)){
            if(!dto.getFirstName().equals(cc.getFirstName()) && !dto.getLastName().equalsIgnoreCase(cc.getLastName())){
                throw new UserException(49, "This Id Card is not yours");
            }
        } else if(cs != null){
            throw new NotFoundException(44, "ID Card Id Card has been used");
        }
        return true;
    }

    private boolean phoneNumberValidation(String phoneNumber, CustomerDTO dto) throws UserException, NotFoundException{
        Customer cs = customerRepo.findByPhoneNumber(phoneNumber);
        Provider pv = providerService.getByPhoneNumber(phoneNumber);
        if (dto.getPhoneNumber() == null || dto.getPhoneNumber().equalsIgnoreCase(" ")){
            throw new UserException(24, "Phone Number column must be filled");
        }
        if(pv.getUtilization().equals(0)){
            if(!dto.getFirstName().equals(pv.getFirstName()) && !dto.getLastName().equalsIgnoreCase(pv.getLastName())){
                throw new UserException(49, "Phone Number Card is not yours");
            }
        } else if(cs != null){
            throw new NotFoundException(44, "Phone Number has been used");
        }
        return true;
    }

    private boolean emailValidation(String email) throws UserException{
        Customer ce = customerRepo.findByEmail(email);
        if (ce != null){
            throw new UserException(49, "Email has been used");
        }
        return true;
    }

    private boolean usernameValidation(String username) throws UserException{
        Customer cu = customerRepo.findByUsername(username);
        if (cu != null){
            throw new UserException(49, "Username has been used");
        }
        return true;
    }

    private boolean npwpValidation(String npwp, CustomerDTO dto) throws UserException, NotFoundException {
        Customer cs = customerRepo.findByNpwp(npwp);
        Npwp cp = npwpService.getById(npwp);
        if (dto.getNpwp() == null || dto.getNpwp().equalsIgnoreCase(" ")){
            throw new UserException(24, "NPWP column must be filled");
        }
        if(cp.getUtilization().equals(0)){
            if(!dto.getFirstName().equals(cp.getFirstName()) && !dto.getLastName().equalsIgnoreCase(cp.getLastName())){
                throw new UserException(49, "This NPWP is not yours");
            }
        } else if(cs != null){
            throw new NotFoundException(44, "NPWP has been used");
        }
        return true;
    }

    private Picture savePicture(Customer cs){
        Picture picture = new Picture();
        cs.setCif(getCif(cs.getFirstName()));
        BigInteger id = pictureRepo.countPicture();
        picture.setIdPicture("P" + id.toString());
        picture.setCif(cs);
        picture.setFileName(null);
        picture.setPicture(null);
        return pictureRepo.save(picture);
    }

    private boolean cashTag(String cashTag) throws UserException{
        Customer cs = customerRepo.findByCashTag(cashTag);
        if (cs != null){
            throw new UserException(49, "Cash Tag has been used");
        }
        return true;
    }

    @Override
    public Customer register(CustomerDTO dto) throws UserException, NotFoundException {
        Customer newCustomer = setCustomer(dto);
        List<Customer> customerValidation = customerRepo.userValidation(dto.getFirstName(), dto.getLastName(), dto.getMother(), dto.getBirthDate());
        if(!customerValidation.isEmpty()){
            throw new UserException(49, "User has been created");
        } else {
            idCardValidation(dto.getIdCard(), dto);
            emailValidation(dto.getEmail());
            usernameValidation(dto.getUsername());
            cashTag(dto.getCashTag());
            npwpValidation(dto.getNpwp(), dto);
            phoneNumberValidation(dto.getPhoneNumber(), dto);
            savePicture(newCustomer);
            newCustomer = customerRepo.save(newCustomer);
        }
        return newCustomer;
    }

    @Override
    public Customer getListByCif(String cif) throws NotFoundException {
        Customer customer = customerRepo.findByCif(cif);
        if(customer == null){
            throw  new NotFoundException(45, "Customer is not found");
        }
        return customer;
    }

    @Override
    public Customer update(UpdateCustomerDTO dto) throws UserException {
        emailValidation(dto.getEmail());
        cashTag(dto.getCashTag());

        Customer update = customerRepo.updateCustomer(dto.getNickName(), dto.getEmail(), dto.getCashTag(), dto.getPhoneNumber());
        return update;
    }

    @Override
    public List<Customer> login(String email, String username, String password) throws NotFoundException {
        List<Customer> login = customerRepo.login(email, username, password);
        if(login == null){
            throw new NotFoundException (44, "username or password is wrong!");
        }
        return login;
    }


}
