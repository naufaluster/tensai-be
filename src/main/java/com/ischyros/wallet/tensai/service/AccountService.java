package com.ischyros.wallet.tensai.service;

import com.ischyros.wallet.tensai.config.exception.UserException;
import com.ischyros.wallet.tensai.dto.AccountDTO;
import com.ischyros.wallet.tensai.model.Account;

public interface AccountService {

    Account register(AccountDTO accountDTO) throws UserException;

}
