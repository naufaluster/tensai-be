package com.ischyros.wallet.tensai.service;

import com.ischyros.wallet.tensai.config.exception.NotFoundException;
import com.ischyros.wallet.tensai.model.Npwp;

public interface NpwpService {

    Npwp getById(String id) throws NotFoundException;
}
