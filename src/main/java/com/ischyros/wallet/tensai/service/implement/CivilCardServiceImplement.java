package com.ischyros.wallet.tensai.service.implement;

import com.ischyros.wallet.tensai.config.exception.NotFoundException;
import com.ischyros.wallet.tensai.model.CivilCard;
import com.ischyros.wallet.tensai.repository.CivilCardRepository;
import com.ischyros.wallet.tensai.service.CivilCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CivilCardServiceImplement implements CivilCardService {

    @Autowired
    private CivilCardRepository civilCardRepo;

    @Override
    public CivilCard getById(String id) throws NotFoundException {
        CivilCard cc = civilCardRepo.findByIdCard(id);
        if(cc == null){
            throw new NotFoundException(44, "ID Card does not exist in this world");
        }
        return cc;
    }
}
