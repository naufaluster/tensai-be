package com.ischyros.wallet.tensai.service;

import com.ischyros.wallet.tensai.model.Role;

import java.util.List;

public interface RoleService {

    List<Role> getList();

}
