package com.ischyros.wallet.tensai.service;

import com.ischyros.wallet.tensai.config.exception.NotFoundException;
import com.ischyros.wallet.tensai.model.CivilCard;

public interface CivilCardService {

    CivilCard getById(String id) throws NotFoundException;
}
