package com.ischyros.wallet.tensai.service;

import com.ischyros.wallet.tensai.config.exception.NotFoundException;
import com.ischyros.wallet.tensai.config.exception.UserException;
import com.ischyros.wallet.tensai.dto.CustomerDTO;
import com.ischyros.wallet.tensai.dto.UpdateCustomerDTO;
import com.ischyros.wallet.tensai.model.Customer;

import java.util.List;

public interface CustomerService {

    // register customer
    Customer register(CustomerDTO dto) throws UserException, NotFoundException;

    // get result by cif
    Customer getListByCif(String cif) throws NotFoundException;

    // update customer
    Customer update(UpdateCustomerDTO dto) throws UserException;

    // login customer
    List<Customer> login(String email, String username, String password) throws NotFoundException;
}
