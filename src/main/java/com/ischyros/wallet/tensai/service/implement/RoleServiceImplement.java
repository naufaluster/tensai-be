package com.ischyros.wallet.tensai.service.implement;

import com.ischyros.wallet.tensai.model.Role;
import com.ischyros.wallet.tensai.repository.RoleRepository;
import com.ischyros.wallet.tensai.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImplement implements RoleService {

    @Autowired
    private RoleRepository roleRepo;

    @Override
    public List<Role> getList() {
        return roleRepo.findAll();
    }
}
