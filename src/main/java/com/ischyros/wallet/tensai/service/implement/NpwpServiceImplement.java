package com.ischyros.wallet.tensai.service.implement;

import com.ischyros.wallet.tensai.config.exception.NotFoundException;
import com.ischyros.wallet.tensai.model.Npwp;
import com.ischyros.wallet.tensai.repository.NpwpRepository;
import com.ischyros.wallet.tensai.service.NpwpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NpwpServiceImplement implements NpwpService {

    @Autowired
    private NpwpRepository repo;

    @Override
    public Npwp getById(String id) throws NotFoundException {
        Npwp npwp = repo.findByNpwp(id);
        if(npwp == null){
            throw new NotFoundException(44, "ID Card does not exist in this world");
        }
        return npwp;
    }

}
