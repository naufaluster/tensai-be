package com.ischyros.wallet.tensai.service;

import com.ischyros.wallet.tensai.config.exception.NotFoundException;
import com.ischyros.wallet.tensai.model.Provider;

public interface ProviderService {

    Provider getByPhoneNumber(String phoneNumber) throws NotFoundException;
}
