package com.ischyros.wallet.tensai.controller;

import com.ischyros.wallet.tensai.config.CommonResponse;
import com.ischyros.wallet.tensai.config.exception.NotFoundException;
import com.ischyros.wallet.tensai.config.exception.UserException;
import com.ischyros.wallet.tensai.dto.CustomerDTO;
import com.ischyros.wallet.tensai.dto.LoginDTO;
import com.ischyros.wallet.tensai.model.Customer;
import com.ischyros.wallet.tensai.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(path = "/customer/{cif}", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<Customer> getListByCif(@PathVariable("cif") String cif) throws NotFoundException {
        if(cif.isEmpty() || cif.equalsIgnoreCase(" ")){
            throw new NotFoundException(24, "Field is empty");
        }
        return new CommonResponse<>(customerService.getListByCif(cif));
    }

    @RequestMapping(path = "/customer", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<Customer> registerCustomer(@RequestBody CustomerDTO dto) throws UserException, NotFoundException {
        Customer customer = customerService.register(dto);
        if(dto.getPassword().length() < 6){
            throw new UserException(34, "Password cannot be less than 6 characters");
        } else if(dto.getPassword().length() > 12){
            throw new UserException(34, "Password cannot be more than 12 characters");
        } else if(dto.getIdCard().isEmpty() || dto.getIdCard().equalsIgnoreCase(" ")){
            throw new NotFoundException(24,  "Id Card column must be filled!");
        } else if(dto.getNpwp().isEmpty() || dto.getNpwp().equalsIgnoreCase(" ")){
            throw new NotFoundException(24, "NPWP column must be filled!");
        } else if(dto.getPhoneNumber().isEmpty() || dto.getPhoneNumber().equalsIgnoreCase(" ")){
            throw new NotFoundException(24, "Phone Number column must be filled!");
        }
        return new CommonResponse<>(21,"Created!", customer);
    }

    @RequestMapping(path = "/customer/login", method = RequestMethod.POST)
    @ResponseBody
    public CommonResponse<List<Customer>> loginCustomer(@RequestBody LoginDTO dto) throws NotFoundException {
        if ((dto.getEmail().isEmpty() && dto.getUsername().isEmpty()) && dto.getPassword().isEmpty() ){
            throw new NotFoundException(24, "Email/Username and Password are empty");
        } else if (dto.getPassword().isEmpty() ){
            throw new NotFoundException(24, "Password is empty");
        } else if (dto.getPassword().length() < 6 ){
            throw new NotFoundException(24, "Password is less than 6 characters");
        } else if (dto.getPassword().length() > 12 ){
            throw new NotFoundException(24, "Password is more than 12 characters");
        } else if (dto.getEmail().isEmpty() && dto.getUsername().isEmpty()){
            throw new NotFoundException (24, "Email/Username is empty");
        }
        return new CommonResponse<>(20, "Login success!", customerService.login(dto.getEmail(), dto.getUsername(), dto.getPassword()));
    }

}
