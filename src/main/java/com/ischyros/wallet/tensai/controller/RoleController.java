package com.ischyros.wallet.tensai.controller;

import com.ischyros.wallet.tensai.config.CommonResponse;
import com.ischyros.wallet.tensai.model.Role;
import com.ischyros.wallet.tensai.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RoleController {

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/role", method = RequestMethod.GET)
    @ResponseBody
    public CommonResponse<List<Role>> getList(){
        List<Role> role = roleService.getList();
        CommonResponse<List<Role>> commonResponse = new CommonResponse<>();
        commonResponse.setData(role);
        return commonResponse;
    }
}
