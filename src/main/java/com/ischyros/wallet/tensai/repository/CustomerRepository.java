package com.ischyros.wallet.tensai.repository;

import com.ischyros.wallet.tensai.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {

    @Query(value = "SELECT COUNT(*) + 1 FROM tensai.customer", nativeQuery = true)
    BigInteger countCustomer();

    // get result by id_card
    @Query(value = "SELECT * FROM tensai.customer WHERE id_card LIKE %:id_card%", nativeQuery = true)
    Customer findByIdCard(
            @Param("id_card") String id_card
    );

    // get result by first name, last name and mother
    @Query(value = "SELECT * FROM tensai.customer WHERE first_name ILIKE %:first_name% AND last_name ILIKE %:last_name% AND mother ILIKE %:mother% AND birth_date LIKE %:birth_date%", nativeQuery = true)
    List<Customer> userValidation(
            @Param("first_name") String first_name,
            @Param("last_name") String last_name,
            @Param("mother") String mother,
            @Param("birth_date") String birth_date
    );

    // get result by email
    @Query(value = "SELECT * FROM tensai.customer WHERE email ILIKE %:email%", nativeQuery = true)
    Customer findByEmail(@Param("email") String email);

    // get result by username
    @Query(value = "SELECT * FROM tensai.customer WHERE username ILIKE %:username%", nativeQuery = true)
    Customer findByUsername(@Param("username") String username);

    // get result by npwp
    @Query(value = "SELECT * FROM tensai.customer WHERE npwp LIKE %:npwp%", nativeQuery = true)
    Customer findByNpwp(@Param("npwp") String npwp);

    // get result by cif
    Customer findByCif(String cif);

    // login
    @Query(value = "SELECT * FROM tensai.customer WHERE email ILIKE %:email% AND username ILIKE %:username% AND password ILIKE %:password%", nativeQuery = true)
    List<Customer> login(
            @Param("email") String email,
            @Param("username") String username,
            @Param("password") String password
    );

    // get result by Cash Tag
    @Query(value = "SELECT * FROM tensai.customer WHERE cash_tag ILIKE %:cashTag%", nativeQuery = true)
    Customer findByCashTag(@Param("cashTag") String cashTag);

    // get for updating
    @Query(value = "SELECT nick_name, cash_tag, email, phone_number FROM tensai.customer WHERE cif = :cif ", nativeQuery = true)
    Customer getUpdateCustomer(@Param("cif") String cif);

    // update cusomer
    @Query(value = "UPDATE tensai.customer SET email = :email, phone_number = :phoneNumber,  cash_tag = :cashTag, nick_name = :nickName WHERE cif = :cif ", nativeQuery = true)
    Customer updateCustomer(
            @Param("nickName") String nickName,
            @Param("email") String email,
            @Param("cashTag") String cashTag,
            @Param("phoneNumber") String phoneNumber
    );


    @Query(value = "SELECT * FROM tensai.customer WHERE phone_number LIKE %:phoneNumber%", nativeQuery = true)
    Customer findByPhoneNumber(@Param("phoneNumber") String phoneNumber);
}
