package com.ischyros.wallet.tensai.repository;

import com.ischyros.wallet.tensai.model.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, String> {

    // get Provider result by Phone Number
    Provider findByPhoneNumber(String phoneNumber);

}
