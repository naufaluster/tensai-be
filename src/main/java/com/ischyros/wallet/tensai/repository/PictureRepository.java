package com.ischyros.wallet.tensai.repository;

import com.ischyros.wallet.tensai.model.Picture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface PictureRepository extends JpaRepository<Picture, String> {

    @Query(value = "SELECT COUNT(*) FROM tensai.picture", nativeQuery = true)
    BigInteger countPicture();
}
