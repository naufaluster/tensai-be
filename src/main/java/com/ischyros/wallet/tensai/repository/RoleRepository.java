package com.ischyros.wallet.tensai.repository;

import com.ischyros.wallet.tensai.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
}
