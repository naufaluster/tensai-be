package com.ischyros.wallet.tensai.repository;

import com.ischyros.wallet.tensai.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface AccountRepository extends JpaRepository<Account, String> {

    @Query(value = "SELECT COUNT(*) + 1 FROM tensai.account", nativeQuery = true)
    BigInteger countByCif();

}
