package com.ischyros.wallet.tensai.repository;

import com.ischyros.wallet.tensai.model.Npwp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface NpwpRepository extends JpaRepository<Npwp, String> {

    // get result by npwp
    @Query(value = "SELECT * FROM tensai.npwp WHERE npwp LIKE %:id%", nativeQuery = true)
    Npwp findByNpwp(String id);
}
