package com.ischyros.wallet.tensai.repository;

import com.ischyros.wallet.tensai.model.CivilCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CivilCardRepository extends JpaRepository<CivilCard, String> {

    // get result by Id Card
    @Query(value = "SELECT * FROM tensai.civil_card WHERE id_card LIKE %:id%", nativeQuery = true)
    CivilCard findByIdCard(@Param("id") String id);
}
