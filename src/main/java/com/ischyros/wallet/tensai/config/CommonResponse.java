package com.ischyros.wallet.tensai.config;


public class CommonResponse<T> {

    private Integer responseCode;
    private String responseMessage;
    private T data;

    public CommonResponse() {
        this.responseCode = 20;
        this.responseMessage = "Success";
    }

    public CommonResponse(T data) {
        this.responseCode = 20;
        this.responseMessage = "Success";
        this.data = data;
    }

    public CommonResponse(Integer responseCode, String responseMessage) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public CommonResponse(Integer responseCode, String responseMessage, T data) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.data = data;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
