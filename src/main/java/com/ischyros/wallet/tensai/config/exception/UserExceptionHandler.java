package com.ischyros.wallet.tensai.config.exception;

import com.ischyros.wallet.tensai.config.CommonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserException.class);
    private static final Logger LOGGER2 = LoggerFactory.getLogger(NotFoundException.class);

    @SuppressWarnings("rawTypes")
    @ExceptionHandler(value = UserException.class)
    public ResponseEntity<CommonResponse> catchUser(UserException e){
        LOGGER.error(e.getMessage());
        LOGGER.warn(e.getMessage());
        LOGGER.info(e.getMessage());
        LOGGER.debug(e.getMessage());
        LOGGER.trace(e.getMessage());
        return new ResponseEntity<>(new CommonResponse(e.getCode(), e.getMessage()), HttpStatus.OK);
    }

    @SuppressWarnings("rawTypes")
    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<CommonResponse> catchNotFound(NotFoundException e){
        LOGGER2.error(e.getMessage());
        LOGGER2.warn(e.getMessage());
        LOGGER2.info(e.getMessage());
        LOGGER2.debug(e.getMessage());
        LOGGER2.trace(e.getMessage());
        return new ResponseEntity<>(new CommonResponse(e.getCode(), e.getMessage()), HttpStatus.OK);
    }

    @SuppressWarnings("rawtypes")
    @ExceptionHandler(value=Exception.class)
    public ResponseEntity<CommonResponse> catchEntityNotFound(Exception e) {
        LOGGER2.error(e.getMessage());
        LOGGER2.warn(e.getMessage());
        LOGGER2.info(e.getMessage());
        LOGGER2.debug(e.getMessage());
        LOGGER2.trace(e.getMessage());
        return new ResponseEntity<>(new CommonResponse(50 , "Entity not found"), HttpStatus.OK);
    }

    @SuppressWarnings("rawtypes")
    @ExceptionHandler(value= HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<CommonResponse> catchEntityNotFound(HttpRequestMethodNotSupportedException e) {
        LOGGER.error(e.getMessage());
        LOGGER.warn(e.getMessage());
        LOGGER.info(e.getMessage());
        LOGGER.debug(e.getMessage());
        LOGGER2.trace(e.getMessage());
        return new ResponseEntity<>(new CommonResponse(45, e.getMessage()), HttpStatus.OK);
    }

}
